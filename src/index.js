import './style.scss';


import Vue from 'vue';

import TestComponent from './components/TestComponent.vue'

const app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
    },
    components: {
        TestComponent
    }
});